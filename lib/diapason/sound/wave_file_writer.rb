require 'wavefile'

class Diapason::Sound::WaveFileWriter
  attr_accessor :sample_rate

  def initialize(sample_rate = 44100)
    @sample_rate = sample_rate.to_f
  end

  def write_samples(samples, path)
    wave_file = WaveFile::Writer.new(path, format(:pcm_16)) do |writer|
      writer.write(buffer(samples))
    end

    File.new(wave_file.file_name)
  end

  def buffer(samples)
    WaveFile::Buffer.new(samples, format(:float))
  end

  def format(sample_format)
    WaveFile::Format.new(:mono, sample_format, sample_rate)
  end
end
