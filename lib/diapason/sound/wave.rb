class Diapason::Sound::Wave
  attr_accessor :frequency, :amplitude

  def initialize(frequency, amplitude = 1)
    @frequency = frequency.to_f
    @amplitude = amplitude.to_f
  end

  def self.from_note(note)
    new(note.frequency)
  end

  def fn(x)
  end
end
