class Diapason::Sound::SineWave < Diapason::Sound::Wave
  def call(x)
    amplitude * Math.sin(x)
  end
end
