class Diapason::Sound::SinusoidalWaveSampler < Diapason::Sound::WaveSampler
  def cycle
    2 * Math::PI
  end
end
