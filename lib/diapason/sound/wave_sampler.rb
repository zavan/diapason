class Diapason::Sound::WaveSampler
  attr_accessor :sample_rate

  def initialize(sample_rate = 44100)
    @sample_rate = sample_rate.to_f
  end

  def each_sample(wave)
    return enum_for(:each_sample, wave) unless block_given?

    x = 0
    loop do
      yield wave.call(x)
      x += cycle * cycles_per_frame(wave.frequency)
    end
  end

  def samples_for_duration(wave, duration)
    return enum_for(:samples_for_duration, wave, duration) unless block_given?

    total_samples = (duration * sample_rate).round

    i = 0
    each_sample(wave) do |sample|
      break if i == total_samples
      i += 1
      yield sample
    end
  end

  def cycles_per_frame(frequency)
    frequency / sample_rate
  end
end
