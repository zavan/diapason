class Diapason::Tuning::EqualTemperament
  include Enumerable

  attr_accessor :partitions, :tonic, :base_size

  def initialize(partitions, tonic, base_size = 2) # 2 = 2:1 = normal octave
    @partitions = partitions
    @tonic = tonic
    @base_size = base_size
  end

  def each
    return enum_for(:each) unless block_given?

    yield tonic

    last_note = tonic

    (partitions - 1).times do
      last_note = Diapason::Note.new(
        last_note.frequency * ratio,
        last_note.index + 1
      )

      yield last_note
    end
  end

  def notes
    each.to_a
  end

  def ratio
    base_size ** (1.0/partitions)
  end
end
