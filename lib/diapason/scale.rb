class Diapason::Scale
  attr_accessor :notes

  def initialize(notes)
    @notes = notes
  end

  def self.build(notes, intervals, tonic)
    scale_notes = [tonic]

    intervals.each do |interval|
      index = notes.index(scale_notes.last) + interval

      if index >= notes.size
        index = notes.size - index
      end

      scale_notes << notes[index]
    end

    new(scale_notes)
  end
end
