module Diapason::Sound
end

require_relative 'sound/wave_sampler'
require_relative 'sound/sinusoidal_wave_sampler'
require_relative 'sound/wave'
require_relative 'sound/sine_wave'
require_relative 'sound/wave_file_writer'
require_relative 'sound/wave_file_player'
