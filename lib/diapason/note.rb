class Diapason::Note
  attr_accessor :frequency, :index

  def initialize(frequency, index)
    @frequency = frequency.to_f
    @index = index
  end

  def self.a440
    new(440, 0)
  end

  def ==(other)
    frequency == other.frequency
  end
end
